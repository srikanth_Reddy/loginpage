document.getElementById('pass').maxLength = '15';
document.getElementById('pass').minLength = '10';


let button = document.querySelector('input[type="submit"]');
button.addEventListener('click', (event) => {
    event.preventDefault();
    let passErr = document.getElementById('passErr');
    let idErr = document.getElementById('idErr');
    let pass = event.target.parentElement.parentElement.password.value;
    let mail = event.target.parentElement.parentElement.mail.value;
    console.log(pass,mail);
    let pattern = new RegExp("(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)");
    if (pattern.test(pass)) {
        passErr.textContent = "";
        fetch('./data.json')
            .then((data) => data.json())
            .then((data) => {
                if (data[mail]) {
                    idErr.textContent = "";
                    if (data[mail] === pass) {
                        window.location.href = "./welcome.html";
                    } else {
                        passErr.textContent = "password not valid";
                    }
                } else {
                    idErr.textContent = 'username not valid';
                }
            })

    } else {
        passErr.textContent = "Must include Upppercass,lowercase,number";
    }


})

